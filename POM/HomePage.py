from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

class HomePage():
    # Constructor
    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(driver, 15)
        # Objetos
        self.barraBusqueda_xpath = "//input[@placeholder='Search and discover music']"
        self.botonBuscar_xpath = "//button[contains(text(),'Search')]//*[local-name()='svg']"
        self.driver.implicitly_wait(10)

    # Métodos
    def escribir_busqueda(self, buscarCosa):
        self.wait.until(ec.visibility_of_element_located((By.XPATH, self.barraBusqueda_xpath)))
        self.driver.find_element_by_xpath(self.barraBusqueda_xpath).clear()
        self.driver.find_element_by_xpath(self.barraBusqueda_xpath).send_keys(buscarCosa)
        self.driver.implicitly_wait(10)

    def click_buscar(self):
        self.wait.until(ec.visibility_of_element_located((By.XPATH, self.botonBuscar_xpath)))
        self.driver.find_element_by_xpath(self.botonBuscar_xpath).click()
        self.driver.implicitly_wait(10)
