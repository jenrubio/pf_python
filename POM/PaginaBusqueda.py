from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

class PaginaBusqueda():
    # Constructor
    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(driver, 15)
        # Objetos
        self.cancion_xpath = "//li[1]//div[1]//div[2]//a[1]"
        self.imagen_xpath = "//li[1]//a[1]//div[1]//img[1]"
        self.todasCanciones_xpath = "//ul[@class='result-items']//li[@class='searchresult band']//div[@class='heading']//a"

    def nombreCancion(self):
        self.wait.until(ec.visibility_of_element_located((By.XPATH, self.cancion_xpath)))
        nombre = self.driver.find_element_by_xpath(self.cancion_xpath).text
        self.driver.implicitly_wait(10)
        return nombre

    def verificarCancionesNombre(self, nombre):
        contar = 0
        verificar = 0
        self.driver.implicitly_wait(15)
        canciones = self.driver.find_elements_by_xpath(self.todasCanciones_xpath)
        for titulo in canciones:
            if nombre in titulo.text.lower():
                contar = contar + 1
        if contar == len(canciones):
            verificar = 1
        self.driver.implicitly_wait(10)
        return verificar

    def clickSugerencia(self):
        self.wait.until(ec.visibility_of_element_located((By.XPATH, self.imagen_xpath)))
        self.driver.implicitly_wait(5)
        self.driver.find_element_by_xpath(self.imagen_xpath).click()
        self.driver.implicitly_wait(10)

