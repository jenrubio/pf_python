from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

class Reproductor():
    # Constructor
    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(driver, 15)
        # Objetos
        self.botonPlay = "//div[@class='playbutton']"
        self.botonPlayOn = "//div[@class='playbutton playing']"

    def click_play(self):
        self.wait.until(ec.visibility_of_element_located((By.XPATH, self.botonPlay)))
        self.driver.find_element_by_xpath(self.botonPlay).click()

    def checar_reproduccion(self):
        resultado = 0
        self.driver.implicitly_wait(15)
        if ec.visibility_of_element_located((By.XPATH, self.botonPlayOn)):
            print("REPRODUCIENDO...")
            resultado = 1
        else:
            resultado = 0
        return resultado