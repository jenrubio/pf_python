# Importamos WebDriver
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
import unittest
import time
from POM.HomePage import HomePage
from POM.PaginaBusqueda import PaginaBusqueda
from POM.Reproductor import Reproductor

class Pruebas(unittest.TestCase):

    def setUp(self) -> None:
        # Referencia de en dónde se encuentra el driver
        self.driver = webdriver.Chrome(executable_path=r"C:\Users\jenrubio\PycharmProjects\ProyectoPython\chromedriver.exe")
        # Hacemos grande la pantalla
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()

    # Método para verificar que el nombre buscado aparece en la primera canción
    def test_nombrePrimeraCancion(self):
        driver = self.driver
        # Declaramos la página
        driver.get("https://bandcamp.com/")
        # Home page
        homeP = HomePage(driver)
        # El nombre lo ingresamos en minúsculas
        homeP.escribir_busqueda("my only one")
        homeP.click_buscar()
        # PaginaBusqueda
        paginaB = PaginaBusqueda(driver)
        # Llamamos a los métodos
        nombre = paginaB.nombreCancion()
        # Si el resultado es 1 = el primer elemento tiene la palabra buscada
        self.assertTrue("my only one", nombre.lower())
        time.sleep(2)

    # Método para verificar que todas las canciones desplegadas tengan la palabra buscada
    def test_listaCancionesNombre(self):
        driver = self.driver
        # Declaramos la página
        driver.get("https://bandcamp.com/")
        # Home page
        homeP = HomePage(driver)
        # Ingresamos el nombre en minusculas
        homeP.escribir_busqueda("my only one")
        homeP.click_buscar()
        # PaginaBusqueda
        paginaB = PaginaBusqueda(driver)
        # Llamamos a los métodos
        resultado = paginaB.verificarCancionesNombre("my only one")
        # Si el resultado es 1 = todos los elementos desplegados tienen la palabra indicada
        self.assertEqual(resultado, 1)
        time.sleep(2)

    # Método para verificar que la canción se está reproduciendo
    def test_reproduccion(self):
        driver = self.driver
        # Declaramos la página
        driver.get("https://bandcamp.com/")
        # HomePage
        homeP = HomePage(driver)
        homeP.escribir_busqueda("my only one")
        homeP.click_buscar()
        # PaginaBusqueda
        paginaB = PaginaBusqueda(driver)
        # Llamamos a los métodos
        paginaB.clickSugerencia()
        # Reproductor
        reproductor = Reproductor(driver)
        # Llamamos los métodos
        reproductor.click_play()
        resultado = reproductor.checar_reproduccion()
        # Si el resultado es 1 = reproduciendo
        self.assertEqual(resultado, 1)
        time.sleep(2)

    def tearDown(self) -> None:
        # Cerramos el driver
        self.driver.implicitly_wait(10)
        self.driver.close()
        self.driver.quit()
        print("Se ha completado la prueba")