# Importamos WebDriver
from selenium import webdriver
import unittest
import time
from POM.HomePage import HomePage
from POM.PaginaBusqueda import PaginaBusqueda
from POM.Reproductor import Reproductor

class Run_ReproductorMusica(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Referencia de en dónde se encuentra el driver
        cls.driver = webdriver.Chrome(executable_path=r"C:\Users\jenrubio\PycharmProjects\ProyectoPython\chromedriver.exe")
        # Hacemos grande la pantalla
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()

    def test_login_valid(self):
        driver = self.driver
        # Declaramos la página
        driver.get("https://bandcamp.com/")

        # HomePage
        homeP = HomePage(driver)
        # Mandamos llamar los métodos de HomePage
        cancion = input("Escribe la canción que deseas buscar: ")
        homeP.escribir_busqueda(cancion.lower())
        homeP.click_buscar()

        # PaginaBusqueda
        paginaB = PaginaBusqueda(driver)
        # Llamamos a los métodos
        nombre = paginaB.nombreCancion()
        print("¿Deseas reproducir esa canción: ", nombre, "? 1) Si, 2) No")
        try:
            valor = int(input("Ingresa el número de acuerdo a tu elección. "))
        except:
            print("Solo se aceptan números enteros.")
        if valor == 1:
            paginaB.clickSugerencia()
            # Reproductor
            reproductor = Reproductor(driver)
            # Llamamos los métodos
            time.sleep(3)
            reproductor.click_play()
            time.sleep(4)
            reproductor.checar_reproduccion()
        else:
            print("adios")
        time.sleep(2)

    @classmethod
    def tearDownClass(cls):
        # Cerramos el driver
        cls.driver.implicitly_wait(10)
        cls.driver.close()
        cls.driver.quit()
        print("Se ha completado la prueba")